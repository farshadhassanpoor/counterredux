import React, {Component} from 'react';
import {View, Button, StyleSheet, Text} from 'react-native';
import CounterControl from './CounterControl';
import CounterOutput from './CounterOutput';
import {connect} from 'react-redux';
import * as actionTypes from '../redux/Counter/Types';

class Counter extends Component {
  render() {
    const {
      ctr,
      onAddCounter,
      onIncrementCounter,
      onMinusCounter,
      onStoreResult,
      onDecrementCounter,
      onDeleteResult,
      rst,
    } = this.props;

    return (
      <View style={styles.container}>
        <CounterOutput value={ctr} />
        <CounterControl label="Increment" clicked={onIncrementCounter} />
        <CounterControl label="Decrement" clicked={onDecrementCounter} />
        <CounterControl label="Add 5" clicked={onAddCounter} />
        <CounterControl label="Subtract 5" clicked={onMinusCounter} />
        <Button title="save results" onPress={onStoreResult} />
        {rst.map(obj => (
          <Text key={obj.id} onPress={() => onDeleteResult(obj.id)}>
            · {obj.value}
          </Text>
        ))}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    ctr: state.counter,
    rst: state.results,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onIncrementCounter: () => dispatch({type: actionTypes.INCREMENT}),
    onDecrementCounter: () => dispatch({type: actionTypes.DECREMENT}),
    onAddCounter: () => dispatch({type: actionTypes.ADD, val: 5}),
    onMinusCounter: () => dispatch({type: actionTypes.MINUS, val: 5}),
    onStoreResult: () => dispatch({type: actionTypes.STORE_RESULT}),
    onDeleteResult: id =>
      dispatch({type: actionTypes.STORE_DELETE, resultElementId: id}),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Counter);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
