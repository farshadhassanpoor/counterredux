import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const counterOutput = props => (
  <View>
    <Text style={styles.output}>Current Counter: {props.value}</Text>
  </View>
);

export default counterOutput;

const styles = StyleSheet.create({
  output: {
    textAlign: 'center',
    borderBottomWidth: 2,
  },
});
