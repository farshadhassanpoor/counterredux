export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const MINUS = 'MINUS';
export const STORE_RESULT = 'STORE_RESULT';
export const STORE_DELETE = 'STORE_DELETE';
